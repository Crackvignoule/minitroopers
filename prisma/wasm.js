
Object.defineProperty(exports, "__esModule", { value: true });

const {
  Decimal,
  objectEnumValues,
  makeStrictEnum,
  Public,
  getRuntime,
} = require('./runtime/index-browser.js')


const Prisma = {}

exports.Prisma = Prisma
exports.$Enums = {}

/**
 * Prisma Client JS version: 5.15.0
 * Query Engine version: 12e25d8d06f6ea5a0252864dd9a03b1bb51f3022
 */
Prisma.prismaVersion = {
  client: "5.15.0",
  engine: "12e25d8d06f6ea5a0252864dd9a03b1bb51f3022"
}

Prisma.PrismaClientKnownRequestError = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`PrismaClientKnownRequestError is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)};
Prisma.PrismaClientUnknownRequestError = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`PrismaClientUnknownRequestError is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.PrismaClientRustPanicError = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`PrismaClientRustPanicError is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.PrismaClientInitializationError = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`PrismaClientInitializationError is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.PrismaClientValidationError = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`PrismaClientValidationError is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.NotFoundError = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`NotFoundError is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.Decimal = Decimal

/**
 * Re-export of sql-template-tag
 */
Prisma.sql = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`sqltag is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.empty = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`empty is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.join = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`join is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.raw = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`raw is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.validator = Public.validator

/**
* Extensions
*/
Prisma.getExtensionContext = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`Extensions.getExtensionContext is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}
Prisma.defineExtension = () => {
  const runtimeName = getRuntime().prettyName;
  throw new Error(`Extensions.defineExtension is unable to run in this browser environment, or has been bundled for the browser (running in ${runtimeName}).
In case this error is unexpected for you, please report it in https://pris.ly/prisma-prisma-bug-report`,
)}

/**
 * Shorthand utilities for JSON filtering
 */
Prisma.DbNull = objectEnumValues.instances.DbNull
Prisma.JsonNull = objectEnumValues.instances.JsonNull
Prisma.AnyNull = objectEnumValues.instances.AnyNull

Prisma.NullTypes = {
  DbNull: objectEnumValues.classes.DbNull,
  JsonNull: objectEnumValues.classes.JsonNull,
  AnyNull: objectEnumValues.classes.AnyNull
}

/**
 * Enums
 */

exports.Prisma.TransactionIsolationLevel = makeStrictEnum({
  ReadUncommitted: 'ReadUncommitted',
  ReadCommitted: 'ReadCommitted',
  RepeatableRead: 'RepeatableRead',
  Serializable: 'Serializable'
});

exports.Prisma.UserScalarFieldEnum = {
  id: 'id',
  lang: 'lang',
  name: 'name',
  createdAt: 'createdAt',
  lastConnexion: 'lastConnexion',
  admin: 'admin',
  connexionToken: 'connexionToken',
  gold: 'gold',
  power: 'power',
  armyName: 'armyName',
  armyUrl: 'armyUrl',
  prefix: 'prefix',
  color: 'color',
  sponsoredById: 'sponsoredById',
  ipAddresses: 'ipAddresses',
  referralGold: 'referralGold'
};

exports.Prisma.RelationLoadStrategy = {
  query: 'query',
  join: 'join'
};

exports.Prisma.HistoryUserScalarFieldEnum = {
  id: 'id',
  ts: 'ts',
  type: 'type',
  options: 'options',
  userId: 'userId'
};

exports.Prisma.TrooperScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  userId: 'userId',
  name: 'name',
  level: 'level',
  weapons: 'weapons',
  skills: 'skills',
  items: 'items',
  vehicle: 'vehicle',
  helmet: 'helmet',
  savedSill1: 'savedSill1',
  savedSill2: 'savedSill2',
  savedSill3: 'savedSill3',
  col0: 'col0',
  col1: 'col1',
  col2: 'col2',
  p0: 'p0',
  p1: 'p1'
};

exports.Prisma.TrooperDayScalarFieldEnum = {
  id: 'id',
  name: 'name',
  level: 'level',
  weapons: 'weapons',
  skills: 'skills',
  items: 'items',
  col0: 'col0',
  col1: 'col1',
  col2: 'col2',
  p0: 'p0',
  p1: 'p1'
};

exports.Prisma.SortOrder = {
  asc: 'asc',
  desc: 'desc'
};

exports.Prisma.NullableJsonNullValueInput = {
  DbNull: Prisma.DbNull,
  JsonNull: Prisma.JsonNull
};

exports.Prisma.QueryMode = {
  default: 'default',
  insensitive: 'insensitive'
};

exports.Prisma.NullsOrder = {
  first: 'first',
  last: 'last'
};

exports.Prisma.JsonNullValueFilter = {
  DbNull: Prisma.DbNull,
  JsonNull: Prisma.JsonNull,
  AnyNull: Prisma.AnyNull
};
exports.Lang = exports.$Enums.Lang = {
  en: 'en',
  fr: 'fr',
  de: 'de',
  es: 'es',
  ru: 'ru',
  pt: 'pt'
};

exports.HistoryType = exports.$Enums.HistoryType = {
  creation: 'creation',
  war: 'war',
  trooperAdd: 'trooperAdd',
  trooperUpdate: 'trooperUpdate',
  trooperAvailable: 'trooperAvailable',
  recruit: 'recruit'
};

exports.HelmetName = exports.$Enums.HelmetName = {
  commsofficer: 'commsofficer',
  doctor: 'doctor',
  munitions: 'munitions',
  pilot: 'pilot',
  saboteur: 'saboteur',
  scout: 'scout',
  soldier: 'soldier',
  spy: 'spy'
};

exports.WeaponName = exports.$Enums.WeaponName = {
  pistol: 'pistol',
  dualpistols: 'dualpistols',
  revolver: 'revolver',
  beretta: 'beretta',
  deserteagle: 'deserteagle',
  ak47: 'ak47',
  m16: 'm16',
  thompson: 'thompson',
  ump: 'ump',
  famas: 'famas',
  assaultrifle: 'assaultrifle',
  semiautoshotgun: 'semiautoshotgun',
  shotgun: 'shotgun',
  pumpactionshotgun: 'pumpactionshotgun',
  doublebarrelledshotgun: 'doublebarrelledshotgun',
  scattergun: 'scattergun',
  sniper: 'sniper',
  mosteck: 'mosteck',
  lizardojungle: 'lizardojungle',
  sparrowhawk: 'sparrowhawk',
  ckmagellan: 'ckmagellan',
  bazookam1: 'bazookam1',
  rocketlauncher: 'rocketlauncher',
  bazookam25: 'bazookam25',
  infernaltube: 'infernaltube',
  comancheauto: 'comancheauto',
  heavymachinegun: 'heavymachinegun',
  galtinggun: 'galtinggun',
  minigun: 'minigun',
  knife: 'knife'
};

exports.PerkName = exports.$Enums.PerkName = {
  unforgiving: 'unforgiving',
  faceboot: 'faceboot',
  fistsoffury: 'fistsoffury',
  wrestler: 'wrestler',
  charge: 'charge',
  smart: 'smart',
  kingofboules: 'kingofboules',
  eyeofthetiger: 'eyeofthetiger',
  coldblooded: 'coldblooded',
  vicious: 'vicious',
  heartbreaker: 'heartbreaker',
  anatomy: 'anatomy',
  blindfury: 'blindfury',
  coveringfire: 'coveringfire',
  nimblefingers: 'nimblefingers',
  juggler: 'juggler',
  unshakable: 'unshakable',
  onpoint: 'onpoint',
  frenetic: 'frenetic',
  vendetta: 'vendetta',
  sprinter: 'sprinter',
  survivalinstinct: 'survivalinstinct',
  enthusiastic: 'enthusiastic',
  adrenaline: 'adrenaline',
  zigzag: 'zigzag',
  rush: 'rush',
  triggerhappy: 'triggerhappy',
  tailgunner: 'tailgunner',
  nervous: 'nervous',
  stamp: 'stamp',
  deathgrip: 'deathgrip',
  lastmohican: 'lastmohican',
  hyperactive: 'hyperactive',
  hurry: 'hurry',
  bounceback: 'bounceback',
  battleready: 'battleready',
  restless: 'restless',
  martyr: 'martyr',
  outofbounds: 'outofbounds',
  reverseattack: 'reverseattack',
  commander: 'commander',
  occupation: 'occupation',
  saviour: 'saviour',
  hardboiled: 'hardboiled',
  dodger: 'dodger',
  tucknroll: 'tucknroll',
  takecover: 'takecover',
  camouflage: 'camouflage',
  hugecalves: 'hugecalves',
  sturdy: 'sturdy',
  bricksteakhouse: 'bricksteakhouse',
  commando: 'commando',
  bait: 'bait',
  heavyweight: 'heavyweight',
  fallguy: 'fallguy',
  invincible: 'invincible',
  suspicious: 'suspicious',
  friendlyfire: 'friendlyfire',
  crybaby: 'crybaby',
  interception: 'interception',
  survivor: 'survivor'
};

exports.ItemName = exports.$Enums.ItemName = {
  explosiveshells: 'explosiveshells',
  hydroshockshells: 'hydroshockshells',
  paralysingshells: 'paralysingshells',
  toxicshells: 'toxicshells',
  armorpiercingshells: 'armorpiercingshells',
  fragmentationgrenade: 'fragmentationgrenade',
  grenade: 'grenade',
  flashbang: 'flashbang',
  gasgrenade: 'gasgrenade',
  gluegrenade: 'gluegrenade',
  shockgrenade: 'shockgrenade',
  clowngrenade: 'clowngrenade',
  grenadebenie: 'grenadebenie',
  healinggrenade: 'healinggrenade',
  blackholegrenade: 'blackholegrenade',
  bulletproofvest: 'bulletproofvest',
  lasersights: 'lasersights',
  luckycharm: 'luckycharm',
  radio: 'radio',
  fullmetalbalaclava: 'fullmetalbalaclava',
  talkywalky: 'talkywalky',
  twinoid: 'twinoid',
  voodoodoll: 'voodoodoll',
  amphetamineshot: 'amphetamineshot',
  rucksack: 'rucksack',
  barrelextension: 'barrelextension',
  binoculars: 'binoculars',
  biped: 'biped',
  wifebeater: 'wifebeater',
  loader: 'loader',
  thermosofcoffee: 'thermosofcoffee',
  scavenger: 'scavenger',
  compensator: 'compensator',
  firstaid: 'firstaid',
  heavyarmor: 'heavyarmor',
  heatsensor: 'heatsensor'
};

exports.VehicleName = exports.$Enums.VehicleName = {
  motorcycle: 'motorcycle',
  lighttank: 'lighttank',
  heavytank: 'heavytank',
  helicopter: 'helicopter',
  fighterjet: 'fighterjet'
};

exports.Prisma.ModelName = {
  User: 'User',
  HistoryUser: 'HistoryUser',
  Trooper: 'Trooper',
  TrooperDay: 'TrooperDay'
};

/**
 * This is a stub Prisma Client that will error at runtime if called.
 */
class PrismaClient {
  constructor() {
    return new Proxy(this, {
      get(target, prop) {
        let message
        const runtime = getRuntime()
        if (runtime.isEdge) {
          message = `PrismaClient is not configured to run in ${runtime.prettyName}. In order to run Prisma Client on edge runtime, either:
- Use Prisma Accelerate: https://pris.ly/d/accelerate
- Use Driver Adapters: https://pris.ly/d/driver-adapters
`;
        } else {
          message = 'PrismaClient is unable to run in this browser environment, or has been bundled for the browser (running in `' + runtime.prettyName + '`).'
        }
        
        message += `
If this is unexpected, please open an issue: https://pris.ly/prisma-prisma-bug-report`

        throw new Error(message)
      }
    })
  }
}

exports.PrismaClient = PrismaClient

Object.assign(exports, Prisma)
