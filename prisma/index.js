
Object.defineProperty(exports, "__esModule", { value: true });

const {
  PrismaClientKnownRequestError,
  PrismaClientUnknownRequestError,
  PrismaClientRustPanicError,
  PrismaClientInitializationError,
  PrismaClientValidationError,
  NotFoundError,
  getPrismaClient,
  sqltag,
  empty,
  join,
  raw,
  Decimal,
  Debug,
  objectEnumValues,
  makeStrictEnum,
  Extensions,
  warnOnce,
  defineDmmfProperty,
  Public,
  getRuntime
} = require('./runtime/library.js')


const Prisma = {}

exports.Prisma = Prisma
exports.$Enums = {}

/**
 * Prisma Client JS version: 5.15.0
 * Query Engine version: 12e25d8d06f6ea5a0252864dd9a03b1bb51f3022
 */
Prisma.prismaVersion = {
  client: "5.15.0",
  engine: "12e25d8d06f6ea5a0252864dd9a03b1bb51f3022"
}

Prisma.PrismaClientKnownRequestError = PrismaClientKnownRequestError;
Prisma.PrismaClientUnknownRequestError = PrismaClientUnknownRequestError
Prisma.PrismaClientRustPanicError = PrismaClientRustPanicError
Prisma.PrismaClientInitializationError = PrismaClientInitializationError
Prisma.PrismaClientValidationError = PrismaClientValidationError
Prisma.NotFoundError = NotFoundError
Prisma.Decimal = Decimal

/**
 * Re-export of sql-template-tag
 */
Prisma.sql = sqltag
Prisma.empty = empty
Prisma.join = join
Prisma.raw = raw
Prisma.validator = Public.validator

/**
* Extensions
*/
Prisma.getExtensionContext = Extensions.getExtensionContext
Prisma.defineExtension = Extensions.defineExtension

/**
 * Shorthand utilities for JSON filtering
 */
Prisma.DbNull = objectEnumValues.instances.DbNull
Prisma.JsonNull = objectEnumValues.instances.JsonNull
Prisma.AnyNull = objectEnumValues.instances.AnyNull

Prisma.NullTypes = {
  DbNull: objectEnumValues.classes.DbNull,
  JsonNull: objectEnumValues.classes.JsonNull,
  AnyNull: objectEnumValues.classes.AnyNull
}


  const path = require('path')

/**
 * Enums
 */
exports.Prisma.TransactionIsolationLevel = makeStrictEnum({
  ReadUncommitted: 'ReadUncommitted',
  ReadCommitted: 'ReadCommitted',
  RepeatableRead: 'RepeatableRead',
  Serializable: 'Serializable'
});

exports.Prisma.UserScalarFieldEnum = {
  id: 'id',
  lang: 'lang',
  name: 'name',
  createdAt: 'createdAt',
  lastConnexion: 'lastConnexion',
  admin: 'admin',
  connexionToken: 'connexionToken',
  gold: 'gold',
  power: 'power',
  armyName: 'armyName',
  armyUrl: 'armyUrl',
  prefix: 'prefix',
  color: 'color',
  sponsoredById: 'sponsoredById',
  ipAddresses: 'ipAddresses',
  referralGold: 'referralGold'
};

exports.Prisma.RelationLoadStrategy = {
  query: 'query',
  join: 'join'
};

exports.Prisma.HistoryUserScalarFieldEnum = {
  id: 'id',
  ts: 'ts',
  type: 'type',
  options: 'options',
  userId: 'userId'
};

exports.Prisma.TrooperScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  userId: 'userId',
  name: 'name',
  level: 'level',
  weapons: 'weapons',
  skills: 'skills',
  items: 'items',
  vehicle: 'vehicle',
  helmet: 'helmet',
  savedSill1: 'savedSill1',
  savedSill2: 'savedSill2',
  savedSill3: 'savedSill3',
  col0: 'col0',
  col1: 'col1',
  col2: 'col2',
  p0: 'p0',
  p1: 'p1'
};

exports.Prisma.TrooperDayScalarFieldEnum = {
  id: 'id',
  name: 'name',
  level: 'level',
  weapons: 'weapons',
  skills: 'skills',
  items: 'items',
  col0: 'col0',
  col1: 'col1',
  col2: 'col2',
  p0: 'p0',
  p1: 'p1'
};

exports.Prisma.SortOrder = {
  asc: 'asc',
  desc: 'desc'
};

exports.Prisma.NullableJsonNullValueInput = {
  DbNull: Prisma.DbNull,
  JsonNull: Prisma.JsonNull
};

exports.Prisma.QueryMode = {
  default: 'default',
  insensitive: 'insensitive'
};

exports.Prisma.NullsOrder = {
  first: 'first',
  last: 'last'
};

exports.Prisma.JsonNullValueFilter = {
  DbNull: Prisma.DbNull,
  JsonNull: Prisma.JsonNull,
  AnyNull: Prisma.AnyNull
};
exports.Lang = exports.$Enums.Lang = {
  en: 'en',
  fr: 'fr',
  de: 'de',
  es: 'es',
  ru: 'ru',
  pt: 'pt'
};

exports.HistoryType = exports.$Enums.HistoryType = {
  creation: 'creation',
  war: 'war',
  trooperAdd: 'trooperAdd',
  trooperUpdate: 'trooperUpdate',
  trooperAvailable: 'trooperAvailable',
  recruit: 'recruit'
};

exports.HelmetName = exports.$Enums.HelmetName = {
  commsofficer: 'commsofficer',
  doctor: 'doctor',
  munitions: 'munitions',
  pilot: 'pilot',
  saboteur: 'saboteur',
  scout: 'scout',
  soldier: 'soldier',
  spy: 'spy'
};

exports.WeaponName = exports.$Enums.WeaponName = {
  pistol: 'pistol',
  dualpistols: 'dualpistols',
  revolver: 'revolver',
  beretta: 'beretta',
  deserteagle: 'deserteagle',
  ak47: 'ak47',
  m16: 'm16',
  thompson: 'thompson',
  ump: 'ump',
  famas: 'famas',
  assaultrifle: 'assaultrifle',
  semiautoshotgun: 'semiautoshotgun',
  shotgun: 'shotgun',
  pumpactionshotgun: 'pumpactionshotgun',
  doublebarrelledshotgun: 'doublebarrelledshotgun',
  scattergun: 'scattergun',
  sniper: 'sniper',
  mosteck: 'mosteck',
  lizardojungle: 'lizardojungle',
  sparrowhawk: 'sparrowhawk',
  ckmagellan: 'ckmagellan',
  bazookam1: 'bazookam1',
  rocketlauncher: 'rocketlauncher',
  bazookam25: 'bazookam25',
  infernaltube: 'infernaltube',
  comancheauto: 'comancheauto',
  heavymachinegun: 'heavymachinegun',
  galtinggun: 'galtinggun',
  minigun: 'minigun',
  knife: 'knife'
};

exports.PerkName = exports.$Enums.PerkName = {
  unforgiving: 'unforgiving',
  faceboot: 'faceboot',
  fistsoffury: 'fistsoffury',
  wrestler: 'wrestler',
  charge: 'charge',
  smart: 'smart',
  kingofboules: 'kingofboules',
  eyeofthetiger: 'eyeofthetiger',
  coldblooded: 'coldblooded',
  vicious: 'vicious',
  heartbreaker: 'heartbreaker',
  anatomy: 'anatomy',
  blindfury: 'blindfury',
  coveringfire: 'coveringfire',
  nimblefingers: 'nimblefingers',
  juggler: 'juggler',
  unshakable: 'unshakable',
  onpoint: 'onpoint',
  frenetic: 'frenetic',
  vendetta: 'vendetta',
  sprinter: 'sprinter',
  survivalinstinct: 'survivalinstinct',
  enthusiastic: 'enthusiastic',
  adrenaline: 'adrenaline',
  zigzag: 'zigzag',
  rush: 'rush',
  triggerhappy: 'triggerhappy',
  tailgunner: 'tailgunner',
  nervous: 'nervous',
  stamp: 'stamp',
  deathgrip: 'deathgrip',
  lastmohican: 'lastmohican',
  hyperactive: 'hyperactive',
  hurry: 'hurry',
  bounceback: 'bounceback',
  battleready: 'battleready',
  restless: 'restless',
  martyr: 'martyr',
  outofbounds: 'outofbounds',
  reverseattack: 'reverseattack',
  commander: 'commander',
  occupation: 'occupation',
  saviour: 'saviour',
  hardboiled: 'hardboiled',
  dodger: 'dodger',
  tucknroll: 'tucknroll',
  takecover: 'takecover',
  camouflage: 'camouflage',
  hugecalves: 'hugecalves',
  sturdy: 'sturdy',
  bricksteakhouse: 'bricksteakhouse',
  commando: 'commando',
  bait: 'bait',
  heavyweight: 'heavyweight',
  fallguy: 'fallguy',
  invincible: 'invincible',
  suspicious: 'suspicious',
  friendlyfire: 'friendlyfire',
  crybaby: 'crybaby',
  interception: 'interception',
  survivor: 'survivor'
};

exports.ItemName = exports.$Enums.ItemName = {
  explosiveshells: 'explosiveshells',
  hydroshockshells: 'hydroshockshells',
  paralysingshells: 'paralysingshells',
  toxicshells: 'toxicshells',
  armorpiercingshells: 'armorpiercingshells',
  fragmentationgrenade: 'fragmentationgrenade',
  grenade: 'grenade',
  flashbang: 'flashbang',
  gasgrenade: 'gasgrenade',
  gluegrenade: 'gluegrenade',
  shockgrenade: 'shockgrenade',
  clowngrenade: 'clowngrenade',
  grenadebenie: 'grenadebenie',
  healinggrenade: 'healinggrenade',
  blackholegrenade: 'blackholegrenade',
  bulletproofvest: 'bulletproofvest',
  lasersights: 'lasersights',
  luckycharm: 'luckycharm',
  radio: 'radio',
  fullmetalbalaclava: 'fullmetalbalaclava',
  talkywalky: 'talkywalky',
  twinoid: 'twinoid',
  voodoodoll: 'voodoodoll',
  amphetamineshot: 'amphetamineshot',
  rucksack: 'rucksack',
  barrelextension: 'barrelextension',
  binoculars: 'binoculars',
  biped: 'biped',
  wifebeater: 'wifebeater',
  loader: 'loader',
  thermosofcoffee: 'thermosofcoffee',
  scavenger: 'scavenger',
  compensator: 'compensator',
  firstaid: 'firstaid',
  heavyarmor: 'heavyarmor',
  heatsensor: 'heatsensor'
};

exports.VehicleName = exports.$Enums.VehicleName = {
  motorcycle: 'motorcycle',
  lighttank: 'lighttank',
  heavytank: 'heavytank',
  helicopter: 'helicopter',
  fighterjet: 'fighterjet'
};

exports.Prisma.ModelName = {
  User: 'User',
  HistoryUser: 'HistoryUser',
  Trooper: 'Trooper',
  TrooperDay: 'TrooperDay'
};
/**
 * Create the Client
 */
const config = {
  "generator": {
    "name": "client",
    "provider": {
      "fromEnvVar": null,
      "value": "prisma-client-js"
    },
    "output": {
      "value": "D:\\yes\\Minitroopers\\prisma",
      "fromEnvVar": null
    },
    "config": {
      "engineType": "library"
    },
    "binaryTargets": [
      {
        "fromEnvVar": null,
        "value": "windows",
        "native": true
      },
      {
        "fromEnvVar": null,
        "value": "debian-openssl-3.0.x"
      },
      {
        "fromEnvVar": null,
        "value": "debian-openssl-1.1.x"
      }
    ],
    "previewFeatures": [
      "nativeDistinct",
      "relationJoins"
    ],
    "isCustomOutput": true
  },
  "relativeEnvPaths": {
    "rootEnvPath": null,
    "schemaEnvPath": "../server/.env"
  },
  "relativePath": "../server/prisma",
  "clientVersion": "5.15.0",
  "engineVersion": "12e25d8d06f6ea5a0252864dd9a03b1bb51f3022",
  "datasourceNames": [
    "db"
  ],
  "activeProvider": "postgresql",
  "inlineDatasources": {
    "db": {
      "url": {
        "fromEnvVar": "DATABASE_URL",
        "value": null
      }
    }
  },
  "inlineSchema": "// This is your Prisma schema file,\n// learn more about it in the docs: https://pris.ly/d/prisma-schema\n\n// Looking for ways to speed up your queries, or scale easily with your serverless or edge functions?\n// Try Prisma Accelerate: https://pris.ly/cli/accelerate-init\n\ngenerator client {\n  provider        = \"prisma-client-js\"\n  output          = \"../../prisma\"\n  previewFeatures = [\"nativeDistinct\", \"relationJoins\"]\n  binaryTargets   = [\"native\", \"debian-openssl-3.0.x\", \"debian-openssl-1.1.x\"]\n}\n\ndatasource db {\n  provider = \"postgresql\"\n  url      = env(\"DATABASE_URL\")\n}\n\nenum Lang {\n  en\n  fr\n  de\n  es\n  ru\n  pt\n}\n\n// https://minitroopers.fandom.com/wiki/Skill_List\nenum WeaponName {\n  // Handguns\n  pistol\n  dualpistols\n  revolver\n  beretta\n  deserteagle\n  // Assault Weapons\n  ak47\n  m16\n  thompson\n  ump\n  famas\n  assaultrifle\n  // Shotguns\n  semiautoshotgun\n  shotgun\n  pumpactionshotgun\n  doublebarrelledshotgun\n  scattergun\n  // Rifles\n  sniper\n  mosteck\n  lizardojungle\n  sparrowhawk\n  ckmagellan\n  // Launchers\n  bazookam1\n  rocketlauncher\n  bazookam25\n  infernaltube\n  // Machine Guns\n  comancheauto\n  heavymachinegun\n  galtinggun\n  minigun\n  // Melee\n  knife\n}\n\n// https://minitroopers.fandom.com/wiki/Skill_List\nenum HelmetName {\n  commsofficer\n  doctor\n  munitions\n  pilot\n  saboteur\n  scout\n  soldier\n  spy\n}\n\n// https://minitroopers.fandom.com/wiki/Skill_List\nenum VehicleName {\n  motorcycle\n  lighttank\n  heavytank\n  helicopter\n  fighterjet\n}\n\n// https://minitroopers.fandom.com/wiki/Skill_List\nenum PerkName {\n  // Others\n  unforgiving\n  faceboot\n  fistsoffury\n  wrestler\n  charge\n  smart\n  kingofboules\n  eyeofthetiger\n  coldblooded\n  vicious\n  heartbreaker\n  anatomy\n  blindfury\n  coveringfire\n  nimblefingers\n  juggler\n  unshakable\n  onpoint\n  frenetic\n  vendetta\n  sprinter\n  survivalinstinct\n  enthusiastic\n  adrenaline\n  zigzag\n  rush\n  triggerhappy\n  tailgunner\n  nervous\n  stamp\n  deathgrip\n  lastmohican\n  hyperactive\n  hurry\n  bounceback\n  battleready\n  restless\n  martyr\n  outofbounds\n  reverseattack\n  commander\n  occupation\n  saviour\n  hardboiled\n  dodger\n  tucknroll\n  takecover\n  camouflage\n  hugecalves\n  sturdy\n  bricksteakhouse\n  commando\n  bait\n  heavyweight\n  fallguy\n  invincible\n  suspicious\n  friendlyfire\n  crybaby\n  interception\n  survivor\n}\n\nenum ItemName {\n  // Ammunition\n  explosiveshells\n  hydroshockshells\n  paralysingshells\n  toxicshells\n  armorpiercingshells\n  // Grenades\n  fragmentationgrenade\n  grenade\n  flashbang\n  gasgrenade\n  gluegrenade\n  shockgrenade\n  clowngrenade\n  grenadebenie\n  healinggrenade\n  blackholegrenade\n  // Others  \n  bulletproofvest\n  lasersights\n  luckycharm\n  radio\n  fullmetalbalaclava\n  talkywalky\n  twinoid\n  voodoodoll\n  amphetamineshot\n  rucksack\n  barrelextension\n  binoculars\n  biped\n  wifebeater\n  loader\n  thermosofcoffee\n  scavenger\n  compensator\n  firstaid\n  heavyarmor\n  heatsensor\n}\n\nmodel User {\n  id             String        @id @unique @db.Uuid\n  lang           Lang          @default(fr)\n  name           String        @db.VarChar(255)\n  createdAt      DateTime      @default(now()) @db.Timestamptz()\n  lastConnexion  DateTime      @default(now()) @db.Timestamptz()\n  admin          Boolean       @default(false)\n  connexionToken String        @db.Uuid\n  gold           Int           @default(0)\n  power          Int           @default(0)\n  armyName       String        @db.VarChar(255)\n  armyUrl        String        @db.VarChar(255) //to remove ?\n  prefix         Int           @default(0)\n  color          Int           @default(0)\n  sponsoredBy    User?         @relation(\"UserSponsor\", fields: [sponsoredById], references: [id])\n  sponsoredById  String?       @db.Uuid\n  sponsoredUsers User[]        @relation(\"UserSponsor\")\n  ipAddresses    String[]      @default([])\n  referralGold   Int           @default(100)\n  troopers       Trooper[]\n  history        HistoryUser[]\n  // + fight\n}\n\nenum HistoryType {\n  creation\n  war\n  trooperAdd\n  trooperUpdate\n  trooperAvailable\n  recruit\n}\n\nmodel HistoryUser {\n  id      String      @id @unique @default(dbgenerated(\"gen_random_uuid()\")) @db.Uuid\n  ts      DateTime    @default(now()) @db.Timestamptz()\n  type    HistoryType\n  options Json?\n  User    User        @relation(fields: [userId], references: [id])\n  userId  String      @db.Uuid\n}\n\nmodel Trooper {\n  id         String        @id @unique @default(dbgenerated(\"gen_random_uuid()\")) @db.Uuid\n  createdAt  DateTime      @default(now()) @db.Timestamptz()\n  User       User          @relation(fields: [userId], references: [id])\n  userId     String        @db.Uuid\n  name       String        @db.VarChar(255)\n  level      Int           @default(1)\n  weapons    WeaponName[]  @default([])\n  skills     PerkName[]    @default([])\n  items      ItemName[]    @default([])\n  vehicle    VehicleName[] @default([])\n  helmet     HelmetName?\n  savedSill1 String?\n  savedSill2 String?\n  savedSill3 String?\n  col0       String        @db.VarChar(7) // Skin [\"#f2bca2\", \"#b4735a\"]\n  col1       String        @db.VarChar(7) // Hair\n  col2       String        @default(\"#391e1c\") @db.VarChar(7) // eyes\n  p0         Int // Hair [0-11]\n  p1         Int // Eyes [0-3]\n  // +pref gun, pref target, ...\n}\n\nmodel TrooperDay {\n  id      String       @id @unique @default(dbgenerated(\"gen_random_uuid()\")) @db.Uuid\n  name    String       @db.VarChar(255)\n  level   Int          @default(1)\n  weapons WeaponName[] @default([])\n  skills  PerkName[]   @default([])\n  items   ItemName[]   @default([])\n  col0    String       @db.VarChar(7) // Skin [\"#f2bca2\", \"#b4735a\"]\n  col1    String       @db.VarChar(7) // Hair\n  col2    String       @default(\"#391e1c\") @db.VarChar(7) // eyes\n  p0      Int // Hair [0-11]\n  p1      Int // Eyes [0-3]\n}\n",
  "inlineSchemaHash": "54e0856991a3543b29c1185d570bbc12360a2d327c5eadd524a02243e93dd3e2",
  "copyEngine": true
}

const fs = require('fs')

config.dirname = __dirname
if (!fs.existsSync(path.join(__dirname, 'schema.prisma'))) {
  const alternativePaths = [
    "../prisma",
    "prisma",
  ]
  
  const alternativePath = alternativePaths.find((altPath) => {
    return fs.existsSync(path.join(process.cwd(), altPath, 'schema.prisma'))
  }) ?? alternativePaths[0]

  config.dirname = path.join(process.cwd(), alternativePath)
  config.isBundled = true
}

config.runtimeDataModel = JSON.parse("{\"models\":{\"User\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"lang\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Lang\",\"default\":\"fr\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"lastConnexion\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"admin\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Boolean\",\"default\":false,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"connexionToken\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gold\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"power\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"armyName\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"armyUrl\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"prefix\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"color\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"sponsoredBy\",\"kind\":\"object\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"UserSponsor\",\"relationFromFields\":[\"sponsoredById\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"sponsoredById\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"sponsoredUsers\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"UserSponsor\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"ipAddresses\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"referralGold\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":100,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"troopers\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Trooper\",\"relationName\":\"TrooperToUser\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"history\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"HistoryUser\",\"relationName\":\"HistoryUserToUser\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"HistoryUser\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":{\"name\":\"dbgenerated\",\"args\":[\"gen_random_uuid()\"]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"ts\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"type\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"HistoryType\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"options\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Json\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"User\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"HistoryUserToUser\",\"relationFromFields\":[\"userId\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"userId\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Trooper\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":{\"name\":\"dbgenerated\",\"args\":[\"gen_random_uuid()\"]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"User\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"TrooperToUser\",\"relationFromFields\":[\"userId\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"userId\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"level\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":1,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"weapons\",\"kind\":\"enum\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"WeaponName\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"skills\",\"kind\":\"enum\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"PerkName\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"items\",\"kind\":\"enum\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"ItemName\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"vehicle\",\"kind\":\"enum\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"VehicleName\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"helmet\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"HelmetName\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"savedSill1\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"savedSill2\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"savedSill3\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"col0\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"col1\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"col2\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"#391e1c\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"p0\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"p1\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"TrooperDay\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":{\"name\":\"dbgenerated\",\"args\":[\"gen_random_uuid()\"]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"level\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":1,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"weapons\",\"kind\":\"enum\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"WeaponName\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"skills\",\"kind\":\"enum\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"PerkName\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"items\",\"kind\":\"enum\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"ItemName\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"col0\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"col1\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"col2\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"#391e1c\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"p0\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"p1\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false}},\"enums\":{\"Lang\":{\"values\":[{\"name\":\"en\",\"dbName\":null},{\"name\":\"fr\",\"dbName\":null},{\"name\":\"de\",\"dbName\":null},{\"name\":\"es\",\"dbName\":null},{\"name\":\"ru\",\"dbName\":null},{\"name\":\"pt\",\"dbName\":null}],\"dbName\":null},\"WeaponName\":{\"values\":[{\"name\":\"pistol\",\"dbName\":null},{\"name\":\"dualpistols\",\"dbName\":null},{\"name\":\"revolver\",\"dbName\":null},{\"name\":\"beretta\",\"dbName\":null},{\"name\":\"deserteagle\",\"dbName\":null},{\"name\":\"ak47\",\"dbName\":null},{\"name\":\"m16\",\"dbName\":null},{\"name\":\"thompson\",\"dbName\":null},{\"name\":\"ump\",\"dbName\":null},{\"name\":\"famas\",\"dbName\":null},{\"name\":\"assaultrifle\",\"dbName\":null},{\"name\":\"semiautoshotgun\",\"dbName\":null},{\"name\":\"shotgun\",\"dbName\":null},{\"name\":\"pumpactionshotgun\",\"dbName\":null},{\"name\":\"doublebarrelledshotgun\",\"dbName\":null},{\"name\":\"scattergun\",\"dbName\":null},{\"name\":\"sniper\",\"dbName\":null},{\"name\":\"mosteck\",\"dbName\":null},{\"name\":\"lizardojungle\",\"dbName\":null},{\"name\":\"sparrowhawk\",\"dbName\":null},{\"name\":\"ckmagellan\",\"dbName\":null},{\"name\":\"bazookam1\",\"dbName\":null},{\"name\":\"rocketlauncher\",\"dbName\":null},{\"name\":\"bazookam25\",\"dbName\":null},{\"name\":\"infernaltube\",\"dbName\":null},{\"name\":\"comancheauto\",\"dbName\":null},{\"name\":\"heavymachinegun\",\"dbName\":null},{\"name\":\"galtinggun\",\"dbName\":null},{\"name\":\"minigun\",\"dbName\":null},{\"name\":\"knife\",\"dbName\":null}],\"dbName\":null},\"HelmetName\":{\"values\":[{\"name\":\"commsofficer\",\"dbName\":null},{\"name\":\"doctor\",\"dbName\":null},{\"name\":\"munitions\",\"dbName\":null},{\"name\":\"pilot\",\"dbName\":null},{\"name\":\"saboteur\",\"dbName\":null},{\"name\":\"scout\",\"dbName\":null},{\"name\":\"soldier\",\"dbName\":null},{\"name\":\"spy\",\"dbName\":null}],\"dbName\":null},\"VehicleName\":{\"values\":[{\"name\":\"motorcycle\",\"dbName\":null},{\"name\":\"lighttank\",\"dbName\":null},{\"name\":\"heavytank\",\"dbName\":null},{\"name\":\"helicopter\",\"dbName\":null},{\"name\":\"fighterjet\",\"dbName\":null}],\"dbName\":null},\"PerkName\":{\"values\":[{\"name\":\"unforgiving\",\"dbName\":null},{\"name\":\"faceboot\",\"dbName\":null},{\"name\":\"fistsoffury\",\"dbName\":null},{\"name\":\"wrestler\",\"dbName\":null},{\"name\":\"charge\",\"dbName\":null},{\"name\":\"smart\",\"dbName\":null},{\"name\":\"kingofboules\",\"dbName\":null},{\"name\":\"eyeofthetiger\",\"dbName\":null},{\"name\":\"coldblooded\",\"dbName\":null},{\"name\":\"vicious\",\"dbName\":null},{\"name\":\"heartbreaker\",\"dbName\":null},{\"name\":\"anatomy\",\"dbName\":null},{\"name\":\"blindfury\",\"dbName\":null},{\"name\":\"coveringfire\",\"dbName\":null},{\"name\":\"nimblefingers\",\"dbName\":null},{\"name\":\"juggler\",\"dbName\":null},{\"name\":\"unshakable\",\"dbName\":null},{\"name\":\"onpoint\",\"dbName\":null},{\"name\":\"frenetic\",\"dbName\":null},{\"name\":\"vendetta\",\"dbName\":null},{\"name\":\"sprinter\",\"dbName\":null},{\"name\":\"survivalinstinct\",\"dbName\":null},{\"name\":\"enthusiastic\",\"dbName\":null},{\"name\":\"adrenaline\",\"dbName\":null},{\"name\":\"zigzag\",\"dbName\":null},{\"name\":\"rush\",\"dbName\":null},{\"name\":\"triggerhappy\",\"dbName\":null},{\"name\":\"tailgunner\",\"dbName\":null},{\"name\":\"nervous\",\"dbName\":null},{\"name\":\"stamp\",\"dbName\":null},{\"name\":\"deathgrip\",\"dbName\":null},{\"name\":\"lastmohican\",\"dbName\":null},{\"name\":\"hyperactive\",\"dbName\":null},{\"name\":\"hurry\",\"dbName\":null},{\"name\":\"bounceback\",\"dbName\":null},{\"name\":\"battleready\",\"dbName\":null},{\"name\":\"restless\",\"dbName\":null},{\"name\":\"martyr\",\"dbName\":null},{\"name\":\"outofbounds\",\"dbName\":null},{\"name\":\"reverseattack\",\"dbName\":null},{\"name\":\"commander\",\"dbName\":null},{\"name\":\"occupation\",\"dbName\":null},{\"name\":\"saviour\",\"dbName\":null},{\"name\":\"hardboiled\",\"dbName\":null},{\"name\":\"dodger\",\"dbName\":null},{\"name\":\"tucknroll\",\"dbName\":null},{\"name\":\"takecover\",\"dbName\":null},{\"name\":\"camouflage\",\"dbName\":null},{\"name\":\"hugecalves\",\"dbName\":null},{\"name\":\"sturdy\",\"dbName\":null},{\"name\":\"bricksteakhouse\",\"dbName\":null},{\"name\":\"commando\",\"dbName\":null},{\"name\":\"bait\",\"dbName\":null},{\"name\":\"heavyweight\",\"dbName\":null},{\"name\":\"fallguy\",\"dbName\":null},{\"name\":\"invincible\",\"dbName\":null},{\"name\":\"suspicious\",\"dbName\":null},{\"name\":\"friendlyfire\",\"dbName\":null},{\"name\":\"crybaby\",\"dbName\":null},{\"name\":\"interception\",\"dbName\":null},{\"name\":\"survivor\",\"dbName\":null}],\"dbName\":null},\"ItemName\":{\"values\":[{\"name\":\"explosiveshells\",\"dbName\":null},{\"name\":\"hydroshockshells\",\"dbName\":null},{\"name\":\"paralysingshells\",\"dbName\":null},{\"name\":\"toxicshells\",\"dbName\":null},{\"name\":\"armorpiercingshells\",\"dbName\":null},{\"name\":\"fragmentationgrenade\",\"dbName\":null},{\"name\":\"grenade\",\"dbName\":null},{\"name\":\"flashbang\",\"dbName\":null},{\"name\":\"gasgrenade\",\"dbName\":null},{\"name\":\"gluegrenade\",\"dbName\":null},{\"name\":\"shockgrenade\",\"dbName\":null},{\"name\":\"clowngrenade\",\"dbName\":null},{\"name\":\"grenadebenie\",\"dbName\":null},{\"name\":\"healinggrenade\",\"dbName\":null},{\"name\":\"blackholegrenade\",\"dbName\":null},{\"name\":\"bulletproofvest\",\"dbName\":null},{\"name\":\"lasersights\",\"dbName\":null},{\"name\":\"luckycharm\",\"dbName\":null},{\"name\":\"radio\",\"dbName\":null},{\"name\":\"fullmetalbalaclava\",\"dbName\":null},{\"name\":\"talkywalky\",\"dbName\":null},{\"name\":\"twinoid\",\"dbName\":null},{\"name\":\"voodoodoll\",\"dbName\":null},{\"name\":\"amphetamineshot\",\"dbName\":null},{\"name\":\"rucksack\",\"dbName\":null},{\"name\":\"barrelextension\",\"dbName\":null},{\"name\":\"binoculars\",\"dbName\":null},{\"name\":\"biped\",\"dbName\":null},{\"name\":\"wifebeater\",\"dbName\":null},{\"name\":\"loader\",\"dbName\":null},{\"name\":\"thermosofcoffee\",\"dbName\":null},{\"name\":\"scavenger\",\"dbName\":null},{\"name\":\"compensator\",\"dbName\":null},{\"name\":\"firstaid\",\"dbName\":null},{\"name\":\"heavyarmor\",\"dbName\":null},{\"name\":\"heatsensor\",\"dbName\":null}],\"dbName\":null},\"HistoryType\":{\"values\":[{\"name\":\"creation\",\"dbName\":null},{\"name\":\"war\",\"dbName\":null},{\"name\":\"trooperAdd\",\"dbName\":null},{\"name\":\"trooperUpdate\",\"dbName\":null},{\"name\":\"trooperAvailable\",\"dbName\":null},{\"name\":\"recruit\",\"dbName\":null}],\"dbName\":null}},\"types\":{}}")
defineDmmfProperty(exports.Prisma, config.runtimeDataModel)
config.engineWasm = undefined


const { warnEnvConflicts } = require('./runtime/library.js')

warnEnvConflicts({
    rootEnvPath: config.relativeEnvPaths.rootEnvPath && path.resolve(config.dirname, config.relativeEnvPaths.rootEnvPath),
    schemaEnvPath: config.relativeEnvPaths.schemaEnvPath && path.resolve(config.dirname, config.relativeEnvPaths.schemaEnvPath)
})

const PrismaClient = getPrismaClient(config)
exports.PrismaClient = PrismaClient
Object.assign(exports, Prisma)

// file annotations for bundling tools to include these files
path.join(__dirname, "query_engine-windows.dll.node");
path.join(process.cwd(), "../prisma/query_engine-windows.dll.node")

// file annotations for bundling tools to include these files
path.join(__dirname, "libquery_engine-debian-openssl-3.0.x.so.node");
path.join(process.cwd(), "../prisma/libquery_engine-debian-openssl-3.0.x.so.node")

// file annotations for bundling tools to include these files
path.join(__dirname, "libquery_engine-debian-openssl-1.1.x.so.node");
path.join(process.cwd(), "../prisma/libquery_engine-debian-openssl-1.1.x.so.node")
// file annotations for bundling tools to include these files
path.join(__dirname, "schema.prisma");
path.join(process.cwd(), "../prisma/schema.prisma")
