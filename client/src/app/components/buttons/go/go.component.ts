import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-go',
  standalone: true,
  imports: [],
  templateUrl: './go.component.html',
  styleUrl: './go.component.scss',
})
export class GoComponent implements OnInit {
  @Input() state: ButtonState = 'pending';
  image: string = '';

  ngOnInit(): void {
    switch (this.state) {
      case 'lost':
        this.image = '/assets/images/bdefeat.webp';
        break;
      case 'pending':
        this.image = '/assets/images/bgo.gif';
        break;
      case 'won':
        this.image = '/assets/images/bwin.webp';
        break;
    }
  }
}

export type ButtonState = 'pending' | 'lost' | 'won';
