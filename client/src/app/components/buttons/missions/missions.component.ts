import { Component } from '@angular/core';
import { FightComponent } from '../fight/fight.component';
import { CommonModule, DecimalPipe } from '@angular/common';
import { ButtonState, GoComponent } from '../go/go.component';

@Component({
  selector: 'app-missions',
  standalone: true,
  imports: [CommonModule, GoComponent],
  providers: [DecimalPipe],
  templateUrl: './../fight/fight.component.html',
  styleUrls: ['./../fight/fight.component.scss', './missions.component.scss'],
})
export class MissionsComponent extends FightComponent {
  override states: ButtonState[] = ['pending', 'pending', 'pending'];

  override getTryLeft() {
    this.tryLeft =
      'Il te reste ' + this.pendingLeft + ' essais pour la mission du jour !';
  }
}
