import { CommonModule, DecimalPipe } from '@angular/common';
import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ButtonState, GoComponent } from '../go/go.component';
import { Subject, interval, takeUntil } from 'rxjs';

@Component({
  selector: 'app-fight',
  standalone: true,
  imports: [CommonModule, GoComponent],
  providers: [DecimalPipe],
  templateUrl: './fight.component.html',
  styleUrl: './fight.component.scss',
})
export class FightComponent implements OnInit, OnDestroy {
  states: ButtonState[] = ['pending', 'pending', 'pending'];

  pendingLeft: number = 3;
  timeLeft: string = '';
  tryLeft: string = '';

  private decimalPipe = inject(DecimalPipe);
  private destroyed$: Subject<void> = new Subject();

  ngOnInit(): void {
    this.pendingLeft = this.states.filter((x) => x == 'pending').length;

    if (this.pendingLeft == 0) {
      this.buildTimeLeft();
      interval(60000)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.buildTimeLeft();
        });
    } else {
      this.getTryLeft();
    }
  }

  getTryLeft() {
    this.tryLeft =
      'Il te reste ' + this.pendingLeft + " batailles aujourd'hui !";
  }

  buildTimeLeft() {
    const diff = new Date().setHours(23, 59, 59, 0) - Date.now();

    const hours = Math.floor(diff / 1000 / 60 / 60);
    const minutes = Math.floor((diff / 1000 / 60) % 60);

    this.timeLeft =
      this.decimalPipe.transform(hours, '2.0-0') +
      'h ' +
      this.decimalPipe.transform(minutes, '2.0-0') +
      'm';
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
