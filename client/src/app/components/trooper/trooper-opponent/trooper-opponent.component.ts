import { Component, Input } from '@angular/core';
import { Opponent } from 'src/app/pages/opponents/opponents.component';
import { PrefixArmy } from 'src/app/pages/signup/signup.component';
import { TrooperCellComponent } from '../trooper-cell/trooper-cell.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-trooper-opponent',
  standalone: true,
  imports: [TrooperCellComponent, CommonModule],
  templateUrl: './trooper-opponent.component.html',
  styleUrl: './trooper-opponent.component.scss',
})
export class TrooperOpponentComponent {
  @Input() opponent!: Opponent;

  prefixs = PrefixArmy;
}
