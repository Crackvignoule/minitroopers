import { CommonModule } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  inject,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  HelmetName,
  ItemName,
  PerkName,
  Trooper,
  VehicleName,
  WeaponName,
} from '@minitroopers/prisma';
import { SkillName, getUpgradeCost } from '@minitroopers/shared';
import { take } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { TrooperService } from 'src/app/services/trooper.service';
import { CommandButtonComponent } from '../../buttons/command-button/command-button.component';
import { TrooperCellComponent } from '../trooper-cell/trooper-cell.component';
import { TooltipDirective } from 'src/app/directives/tooltip.directive';

@Component({
  selector: 'app-trooper-skills',
  standalone: true,
  imports: [
    CommonModule,
    TrooperCellComponent,
    CommandButtonComponent,
    ReactiveFormsModule,
    TooltipDirective,
  ],
  templateUrl: './trooper-skills.component.html',
  styleUrl: './trooper-skills.component.scss',
})
export class TrooperSkillsComponent implements OnChanges {
  @Input() selectedTrooper!: Trooper;
  @Output() switchButton: EventEmitter<boolean> = new EventEmitter();
  @Output() updatedTrooper: EventEmitter<Trooper> = new EventEmitter();

  public skills: SkillName[] = [];

  public unlockedSkills: Partial<{ [skillName in SkillName]: boolean }> = {};

  public updateForm: FormGroup = new FormGroup({
    preferedWeapon: new FormControl('', Validators.required),
    preferedAim: new FormControl('', Validators.required),
    preferedTarget: new FormControl('', Validators.required),
    preferedBehavior: new FormControl('', Validators.required),
    preferedPriority: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required), //check allowed
  });

  public updates: {
    value: string;
    label: string;
    options: { name: string; value: number }[];
  }[] = [
    {
      value: 'preferedWeapon',
      label: 'Prefered Weapon',
      options: [
        { name: '0', value: 0 },
        { name: '1', value: 1 },
        { name: '2', value: 2 },
      ],
    },
    {
      value: 'preferedAim',
      label: 'Prefered Aim',
      options: [
        { name: '0', value: 0 },
        { name: '1', value: 1 },
        { name: '2', value: 2 },
      ],
    },
    {
      value: 'preferedTarget',
      label: 'Prefered Target',
      options: [
        { name: '0', value: 0 },
        { name: '1', value: 1 },
        { name: '2', value: 2 },
      ],
    },
    {
      value: 'preferedBehavior',
      label: 'Prefered Behavior',
      options: [
        { name: '0', value: 0 },
        { name: '1', value: 1 },
        { name: '2', value: 2 },
      ],
    },
    {
      value: 'preferedPriority',
      label: 'Prefered Priority',
      options: [
        { name: '0', value: 0 },
        { name: '1', value: 1 },
        { name: '2', value: 2 },
      ],
    },
  ];

  public upgradeCost: number = 0;
  public lock: boolean = false;

  private authService = inject(AuthService);
  private trooperService = inject(TrooperService);

  constructor() {
    this.skills = [
      ...Object.values(HelmetName),
      ...Object.values(WeaponName),
      ...[
        ...Object.values(VehicleName),
        ...Object.values(ItemName),
        ...Object.values(PerkName),
      ].sort(),
    ];
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['selectedTrooper']?.currentValue) {
      const allCurrentSKills = [
        this.selectedTrooper.helmet,
        ...this.selectedTrooper.vehicle,
        ...this.selectedTrooper.items,
        ...this.selectedTrooper.weapons,
        ...this.selectedTrooper.skills,
      ].filter((x) => x != null) as SkillName[];

      this.unlockedSkills = {};

      for (const skill of allCurrentSKills) {
        this.unlockedSkills[skill] = true;
      }

      this.upgradeCost = getUpgradeCost(this.selectedTrooper.level);

      this.updateForm.reset();
      this.buildForm();
    }
  }

  buildForm() {
    this.updateForm.get('name')?.setValue(this.selectedTrooper.name);
    this.updateForm.disable();
  }

  payUpgrade() {
    if (this.selectedTrooper.savedSill1) {
      this.switchButton.emit(true);
    } else if (
      this.authService.user &&
      this.authService.user.gold >= this.upgradeCost &&
      !this.lock
    ) {
      this.lock = true;
      this.trooperService
        .upgradeTrooper(this.selectedTrooper.id)
        .pipe(take(1))
        .subscribe((response) => {
          if (response.troopers?.length) {
            const trooperUpdated = response.troopers.find(
              (x) => x.id == this.selectedTrooper.id
            );
            if (trooperUpdated) {
              this.updatedTrooper.emit(trooperUpdated);
              this.switchButton.emit(true);
            }
          }
        });
    } else if (this.lock) {
    } else {
      console.log('Not enought gold');
    }
  }
}
