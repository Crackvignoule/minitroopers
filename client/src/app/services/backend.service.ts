import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { TrooperDay } from '@minitroopers/prisma';
import {
  PartialUserWithTroopers,
  UserWithTroopers,
  statusAvailability,
} from '@minitroopers/shared';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class BackendService {
  private http = inject(HttpClient);

  checkAvailability(armyName: string) {
    let queryParams = new HttpParams();
    queryParams = queryParams.append('name', armyName);
    return this.http
      .get<{ status: statusAvailability }>(
        environment.apiUrl + '/api/util/checkNameAvailability',
        {
          params: queryParams,
        }
      )
      .pipe(map((res: { status: statusAvailability }) => res.status));
  }

  checkArmyExist(armyName: string) {
    let queryParams = new HttpParams();
    queryParams = queryParams.append('name', armyName);
    return this.http
      .get<{ status: boolean }>(
        environment.apiUrl + '/api/util/checkArmyExist',
        {
          params: queryParams,
        }
      )
      .pipe(map((res: { status: boolean }) => res.status));
  }

  getTodayTroopers(): Observable<TrooperDay[]> {
    return this.http.get<TrooperDay[]>(
      environment.apiUrl + '/api/util/getTodayTroopers',
      {}
    );
  }

  createUser(formgroup: {
    army: string;
    prefix: number;
    color: number;
    trooper: string;
    userId: string;
  }): Observable<UserWithTroopers> {
    return this.http.post<UserWithTroopers>(
      environment.apiUrl + '/api/user/create',
      {
        ...formgroup,
      }
    );
  }

  getArmy(army: string) {
    let queryParams = new HttpParams();
    queryParams = queryParams.append('army', army);

    return this.http.get<PartialUserWithTroopers>(
      environment.apiUrl + '/api/user/get',
      {
        params: queryParams,
      }
    );
  }
}
