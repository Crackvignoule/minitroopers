import { Injectable, inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SkillName, UserWithTroopers } from '@minitroopers/shared';
import { tap } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class TrooperService {
  private http = inject(HttpClient);
  private authService = inject(AuthService);

  upgradeTrooper(trooperId: string) {
    return this.http
      .post<UserWithTroopers>(environment.apiUrl + '/api/trooper/upgrade', {
        trooperId: trooperId,
      })
      .pipe(
        tap((response) => {
          if (response?.troopers?.length) {
            this.authService.user = response;
          }
        })
      );
  }

  chooseSkill(trooperId: string, skillName: SkillName) {
    return this.http
      .post<UserWithTroopers>(environment.apiUrl + '/api/trooper/chooseSkill', {
        trooperId: trooperId,
        skillName: skillName,
      })
      .pipe(
        tap((response) => {
          if (response?.troopers?.length) {
            this.authService.user = response;
          }
        })
      );
  }

  addTrooper(trooperId: string) {
    return this.http
      .post<UserWithTroopers>(environment.apiUrl + '/api/trooper/add', {
        trooper: trooperId,
      })
      .pipe(
        tap((response) => {
          if (response?.troopers?.length) {
            this.authService.user = response;
          }
        })
      );
  }
}
