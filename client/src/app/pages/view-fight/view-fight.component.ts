import { Component } from '@angular/core';
import { ContainerComponent } from 'src/app/layouts/container/container.component';

@Component({
  selector: 'app-view-fight',
  standalone: true,
  imports: [ContainerComponent],
  templateUrl: './view-fight.component.html',
  styleUrl: './view-fight.component.scss',
})
export class ViewFightComponent {}
