import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { IconContainerComponent } from 'src/app/components/containers/container-icon/container-icon.component';
import { TroopersBlockComponent } from 'src/app/components/trooper/troopers-block/troopers-block.component';
import { ContainerComponent } from 'src/app/layouts/container/container.component';
import { PrefixArmy } from '../signup/signup.component';
import { TrooperOpponentComponent } from 'src/app/components/trooper/trooper-opponent/trooper-opponent.component';

@Component({
  selector: 'app-opponents',
  standalone: true,
  imports: [
    ContainerComponent,
    IconContainerComponent,
    TroopersBlockComponent,
    TrooperOpponentComponent,
    CommonModule,
  ],
  templateUrl: './opponents.component.html',
  styleUrl: './opponents.component.scss',
})
export class OpponentsComponent implements OnInit {
  public prefixs = PrefixArmy;

  // DEBUG
  user: any = {
    gold: 78,
    power: 56,
    armyName: 'aaaaa',
    armyPrefix: 2 as number,
  };

  opponents: Opponent[] = [
    {
      armyName: 'aaaa',
      armyPrefix: 1,
      armyPower: 10,
      troopers: [
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
      ],
    },
    {
      armyName: 'aaaa',
      armyPrefix: 1,
      armyPower: 10,
      troopers: [
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
      ],
    },
    {
      armyName: 'aaaa',
      armyPrefix: 1,
      armyPower: 10,
      troopers: [
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
      ],
    },
    {
      armyName: 'aaaa',
      armyPrefix: 1,
      armyPower: 10,
      troopers: [
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
      ],
    },
    {
      armyName: 'aaaa',
      armyPrefix: 1,
      armyPower: 10,
      troopers: [
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
        {
          id: 0,
          itemsId: [18, null, null],
          level: 1,
          name: 'Crakust',
          skillsId: [14, 1],
          style: '0454564021454505454',
        },
      ],
    },
  ];

  ngOnInit(): void {
    this.opponents.sort((a, b) => a.troopers.length - b.troopers.length);
  }
}

export interface Opponent {
  armyName: string;
  armyPrefix: number;
  armyPower: number;
  troopers: any[];
}
