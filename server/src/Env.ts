const Env = {
  NODE_ENV: "development",
  PORT: 3000,
  SELF_URL: "http://localhost:4200/",

  ETWIN_URL: "http://localhost:50321/",
  ETWIN_CLIENT_ID: "minitroopers@clients",
  ETWIN_CLIENT_SECRET: "dev",

  DEBUG_QUERIES: true,
};

export default Env;
