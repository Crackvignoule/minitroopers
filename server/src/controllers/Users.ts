import { PrismaClient } from "@minitroopers/prisma";
import { Request, Response } from "express";
import { auth } from "../utils/UserHelper.js";
import { checkNameValide, getReferralPrice } from "@minitroopers/shared";

const Users = {
  create: (prisma: PrismaClient) => async (req: Request, res: Response) => {
    try {
      if (
        req.body.army == null ||
        req.body.color == null ||
        req.body.prefix == null ||
        req.body.trooper == null ||
        req.body.userId == null
      ) {
        throw new Error();
      }

      const user = await auth(prisma, req);

      // Check np troopers
      if (user.troopers.length > 0) {
        throw new Error();
      }

      // Check name validity
      if (typeof req.body.army != "string" || !checkNameValide(req.body.army)) {
        throw new Error();
      }

      // Check color [0-5]
      if (
        !(
          typeof req.body.color === "number" &&
          Number.isInteger(req.body.color) &&
          req.body.color >= 0 &&
          req.body.color <= 5
        )
      ) {
        throw new Error();
      }

      // Check prefix [0-5]
      if (
        !(
          typeof req.body.color === "number" &&
          Number.isInteger(req.body.color) &&
          req.body.prefix >= 0 &&
          req.body.prefix <= 5
        )
      ) {
        throw new Error();
      }

      // Check name already exist
      const existingName = await prisma.user.findFirst({
        where: {
          armyName: { equals: req.body.army as string, mode: "insensitive" },
        },
      });

      if (existingName) {
        throw new Error();
      }

      // Check Today Trooper
      const existingTodayTrooper = await prisma.trooperDay.findFirst({
        where: {
          id: req.body.trooper,
        },
      });

      if (!existingTodayTrooper) {
        throw new Error();
      }

      const newIp = (req.headers["x-forwarded-for"] ??
        req.connection.remoteAddress) as string;

      let refArmy = null;
      if (
        req.body.referralName != null &&
        typeof req.body.referralName === "string" &&
        req.body.referralName != "" &&
        checkNameValide(req.body.referralName)
      ) {
        refArmy = await prisma.user.findFirst({
          where: {
            armyName: {
              equals: req.body.referralName as string,
              mode: "insensitive",
            },
          },
          include: {
            sponsoredUsers: true,
          },
        });

        if (refArmy?.id) {
          await prisma.user.update({
            where: { id: refArmy.id },
            data: {
              gold: {
                increment: refArmy.referralGold,
              },
              history: {
                create: {
                  type: "recruit",
                  options: {
                    armyName: req.body.army,
                    reward: refArmy.referralGold,
                    success: true, // false --> if know ip or limit /day
                  },
                },
              },
              referralGold: getReferralPrice(refArmy.sponsoredUsers.length + 1),
            },
          });
        }
      }

      const army = await prisma.user.update({
        where: { id: user.id },
        data: {
          armyName: req.body.army,
          prefix: req.body.prefix,
          color: req.body.color,
          power: 5,
          ipAddresses: [newIp],
          sponsoredById: refArmy?.id,
          troopers: {
            create: [
              {
                name: existingTodayTrooper.name,
                items: existingTodayTrooper.items,
                skills: existingTodayTrooper.skills,
                weapons: existingTodayTrooper.weapons,
                col0: existingTodayTrooper.col0,
                col1: existingTodayTrooper.col1,
                col2: existingTodayTrooper.col2,
                p0: existingTodayTrooper.p0,
                p1: existingTodayTrooper.p1,
              },
            ],
          },
          history: {
            create: [
              {
                type: "creation",
              },
            ],
          },
        },
        include: {
          troopers: {
            orderBy: {
              createdAt: "asc",
            },
          },
          history: {
            take: 5,
            orderBy: {
              ts: "desc",
            },
          },
        },
      });

      res.send(army);
    } catch (error) {
      res.send({ status: "error" });
    }
  },
  signin: (prisma: PrismaClient) => async (req: Request, res: Response) => {
    try {
      const user = await auth(prisma, req);

      res.send(user);
    } catch (error) {
      res.send({ status: "error" });
    }
  },
  get: (prisma: PrismaClient) => async (req: Request, res: Response) => {
    try {
      if (!req.query.army || typeof req.query.army != "string") {
        throw new Error();
      }

      const name: string = req.query.army;

      if (!checkNameValide(name)) {
        throw new Error();
      }

      const user = await prisma.user.findFirst({
        where: {
          armyName: { equals: name, mode: "insensitive" },
        },
        select: {
          armyName: true,
          color: true,
          gold: true,
          history: {
            take: 5,
            orderBy: {
              ts: "desc",
            },
          },
          power: true,
          prefix: true,
          troopers: {
            orderBy: {
              createdAt: "asc",
            },
          },
        },
      });

      res.send(user);
    } catch (error) {
      res.send({ status: "error" });
    }
  },
};

export default Users;
