import {
  HelmetName,
  ItemName,
  PerkName,
  PrismaClient,
  VehicleName,
  WeaponName,
} from "@minitroopers/prisma";
import { Request, Response } from "express";
import { auth } from "../utils/UserHelper.js";
import {
  getUpgradeCost,
  SkillName,
  pickSkill,
  getAddCost,
} from "@minitroopers/shared";

const Troopers = {
  upgrade: (prisma: PrismaClient) => async (req: Request, res: Response) => {
    try {
      if (!req.body.trooperId || typeof req.body.trooperId != "string") {
        throw new Error();
      }

      const user = await auth(prisma, req);

      if (!user) {
        throw new Error();
      }

      const trooper = user.troopers.find((x) => x.id === req.body.trooperId);

      if (!trooper) {
        throw new Error();
      }

      if (trooper.savedSill1) {
        res.send();
        return;
      }

      const upgradeCost = getUpgradeCost(trooper.level);

      if (user.gold < upgradeCost) {
        throw new Error();
      }

      let i: number = 0;

      const skill1 = pickSkill(trooper);
      if (!skill1) {
        throw new Error();
      }

      let skill2;
      do {
        skill2 = pickSkill(trooper);
        i++;
        if (!skill2) {
          throw new Error();
        }
      } while (skill1 === skill2 && i < 500);

      let skill3;
      if (trooper.skills.includes(PerkName.smart)) {
        do {
          skill3 = pickSkill(trooper);
          i++;
          if (!skill3) {
            throw new Error();
          }
        } while ((skill3 === skill2 || skill3 === skill1) && i < 500);
      }

      await prisma.trooper.update({
        where: {
          id: trooper.id,
        },
        data: {
          savedSill1: skill1,
          savedSill2: skill2,
          savedSill3: skill3,
          level: {
            increment: 1,
          },
        },
      });

      const userUpdated = await prisma.user.update({
        where: {
          id: user.id,
        },
        data: {
          gold: user.gold - upgradeCost,
          power: { increment: 1 },
        },
        include: {
          troopers: {
            orderBy: {
              createdAt: "asc",
            },
          },
          history: {
            take: 5,
            orderBy: {
              ts: "desc",
            },
          },
        },
      });

      res.send(userUpdated);
    } catch (error) {
      res.send({ status: "error" });
    }
  },

  chooseSkill:
    (prisma: PrismaClient) => async (req: Request, res: Response) => {
      try {
        if (!req.body.trooperId || typeof req.body.trooperId != "string") {
          throw new Error();
        }
        if (
          !req.body.skillName ||
          typeof req.body.skillName != "string" ||
          ![
            ...Object.values(WeaponName),
            ...Object.values(PerkName),
            ...Object.values(ItemName),
            ...Object.values(VehicleName),
            ...Object.values(HelmetName),
          ].includes(req.body.skillName)
        ) {
          throw new Error();
        }

        const user = await auth(prisma, req);

        if (!user) {
          throw new Error();
        }

        const trooper = user.troopers.find((x) => x.id === req.body.trooperId);

        if (!trooper) {
          throw new Error();
        }

        const skill = req.body.skillName;

        if (skill in WeaponName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              weapons: {
                push: skill,
              },
            },
          });
        } else if (skill in ItemName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              items: {
                push: skill,
              },
            },
          });
        } else if (skill in HelmetName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              helmet: skill,
            },
          });
        } else if (skill in VehicleName) {
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              vehicle: { push: skill },
            },
          });
        } else {
          // Perk
          await prisma.trooper.update({
            where: {
              id: trooper.id,
            },
            data: {
              savedSill1: null,
              savedSill2: null,
              savedSill3: null,
              skills: {
                push: skill,
              },
            },
          });
        }

        const userUpdated = await prisma.user.findFirst({
          where: {
            id: user.id,
          },
          include: {
            troopers: {
              orderBy: {
                createdAt: "asc",
              },
            },
            history: {
              take: 5,
              orderBy: {
                ts: "desc",
              },
            },
          },
        });

        res.send(userUpdated);
      } catch (error) {
        res.send({ status: "error" });
      }
    },

  add: (prisma: PrismaClient) => async (req: Request, res: Response) => {
    try {
      if (!req.body.trooper || typeof req.body.trooper != "string") {
        throw new Error();
      }

      const user = await auth(prisma, req);

      if (!user || user.troopers.length > 11) {
        throw new Error();
      }

      const existingTodayTrooper = await prisma.trooperDay.findFirst({
        where: {
          id: req.body.trooper,
        },
      });

      if (!existingTodayTrooper) {
        throw new Error();
      }

      const goldNeeded = getAddCost(user.troopers.length);

      if (!(goldNeeded > 0)) {
        throw new Error();
      }

      const updatedUser = await prisma.user.update({
        where: { id: user.id },
        data: {
          gold: {
            decrement: goldNeeded,
          },
          power: {
            increment: 5,
          },
          troopers: {
            create: {
              name: existingTodayTrooper.name,
              items: existingTodayTrooper.items,
              skills: existingTodayTrooper.skills,
              weapons: existingTodayTrooper.weapons,
              col0: existingTodayTrooper.col0,
              col1: existingTodayTrooper.col1,
              col2: existingTodayTrooper.col2,
              p0: existingTodayTrooper.p0,
              p1: existingTodayTrooper.p1,
            },
          },
          // history: {
          //   create: [
          //     {
          //       type: "creation",
          //     },
          //   ],
          // },
        },
        include: {
          troopers: {
            orderBy: {
              createdAt: "asc",
            },
          },
          history: {
            take: 5,
            orderBy: {
              ts: "desc",
            },
          },
        },
      });

      res.send(updatedUser);
    } catch (error) {
      res.send({ status: "error" });
    }
  },
};

export default Troopers;
