import { PrismaClient } from "@minitroopers/prisma";
import { Request } from "express";

export const auth = async (prisma: PrismaClient, request: Request) => {
  const {
    headers: { authorization },
  } = request;

  if (!authorization) {
    throw new Error("You are not logged in");
  }
  if (typeof authorization !== "string") {
    throw new Error("Invalid authorization header");
  }

  const [id, token] = Buffer.from(authorization.split(" ")[1], "base64")
    .toString()
    .split(":");

  if (!id || !token || id === "null" || token === "null") {
    throw new Error("Invalid authorization header content");
  }

  const user = await prisma.user.findFirst({
    where: {
      id,
      connexionToken: token,
    },
    include: {
      troopers: {
        orderBy: {
          createdAt: "asc",
        },
      },
      history: {
        take: 5,
        orderBy: {
          ts: "desc",
        },
      },
    },
  });

  if (!user) {
    throw new Error("User not found");
  }
  const newIp =
    request.headers["x-forwarded-for"] ?? request.connection.remoteAddress;
  if (newIp && typeof newIp === "string" && !user.ipAddresses.includes(newIp)) {
    await prisma.user.update({
      where: {
        id: user.id,
      },
      data: {
        lastConnexion: new Date(),
        ipAddresses: {
          push: newIp,
        },
      },
    });
  } else {
    await prisma.user.update({
      where: {
        id: user.id,
      },
      data: {
        lastConnexion: new Date(),
      },
    });
  }

  return user;
};
