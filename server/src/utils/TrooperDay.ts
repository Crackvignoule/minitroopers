import {
  ItemName,
  PrismaClient,
  PerkName,
  WeaponName,
} from "@minitroopers/prisma";
import { Names } from "./Names.js";
import { pickSkill } from "@minitroopers/shared";

export const initTrooperDay = async (prisma: PrismaClient) => {
  await prisma.trooperDay.deleteMany();
  await generateTodayTroopers(prisma);
  console.log("init today trooper");
  return;
};

export const generateTodayTroopers = async (prisma: PrismaClient) => {
  await prisma.trooperDay.deleteMany({});
  let i: number = 0;
  do {
    await createTrooperDay(prisma);
    i++;
  } while (i < 5);
  return;
};

const createTrooperDay = async (prisma: PrismaClient) => {
  await prisma.trooperDay.create({
    data: {
      name: generateRandomName(),
      col0: generateRandomColorSkin(),
      col1: generateRandomColorHair(),
      p0: Math.floor(Math.random() * 12),
      p1: Math.floor(Math.random() * 4),
      ...getInitialSkills(),
    },
  });

  return;
};

const generateRandomName = (): string => {
  const availableNames = Names;
  return availableNames[Math.floor(Math.random() * availableNames.length)];
};

const generateRandomColorSkin = (): string => {
  const colors: string[] = ["#f2bca2", "#b4735a"];
  return colors[Math.floor(Math.random() * colors.length)];
};

const generateRandomColorHair = (): string => {
  const colors: string[] = [
    "#221411",
    "#391e1c",
    "#563229",
    "#724b27",
    "#7b551e",
    "#f07e0f",
    "#fd9b0b",
    "#e8ba06",
    "#f8e754",
  ];
  return colors[Math.floor(Math.random() * colors.length)];
};

const getInitialSkills = (): {
  weapons: WeaponName[];
  skills: PerkName[];
  items: ItemName[];
} => {
  let init: {
    weapons: WeaponName[];
    skills: PerkName[];
    items: ItemName[];
  } = {
    weapons: [],
    skills: [],
    items: [],
  };
  const firstSkill = getRandomWeapon();
  init.weapons = [firstSkill];
  let secondSkill = null;
  do {
    secondSkill = pickSkill();
  } while (secondSkill === firstSkill);

  if (secondSkill in WeaponName) {
    init.weapons.push(secondSkill as WeaponName);
  } else if (secondSkill in ItemName) {
    init.items.push(secondSkill as ItemName);
  } else {
    init.skills.push(secondSkill as PerkName);
  }

  return init;
};

const getRandomWeapon = (): WeaponName => {
  const values = Object.values(WeaponName);
  return values[Math.floor(Math.random() * values.length)];
};
