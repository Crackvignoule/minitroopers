import { ServerReadyResponse } from "@minitroopers/shared";
import { Express, Request, Response } from "express";
import ServerState from "./utils/ServerState.js";
import OAuth from "./controllers/OAuth.js";
import { PrismaClient } from "@minitroopers/prisma";
import Utils from "./controllers/Utils.js";
import Users from "./controllers/Users.js";
import Troopers from "./controllers/Troopers.js";

const initRoutes = (app: Express, prisma: PrismaClient) => {
  // Server state
  app.get("/api", (req: Request, res: Response) =>
    res.status(200).send({
      message: "server is up!",
    })
  );
  app.get(
    "/api/is-ready",
    (req: Request, res: Response<ServerReadyResponse>) => {
      res.status(200).send({
        ready: ServerState.isReady(),
      });
    }
  );

  // OAuth
  app.get("/api/oauth/redirect", OAuth.redirect);
  app.get("/api/oauth/token", OAuth.token(prisma));

  // Utils
  app.get(
    "/api/util/checkNameAvailability",
    Utils.checkNameAvailability(prisma)
  );
  app.get("/api/util/checkArmyExist", Utils.checkArmyExist(prisma));
  app.get("/api/util/getTodayTroopers", Utils.getTodayTrooper(prisma));

  // User
  app.post("/api/user/create", Users.create(prisma));
  app.get("/api/user/signin", Users.signin(prisma));
  app.get("/api/user/get", Users.get(prisma));

  // Trooper
  app.post("/api/trooper/upgrade", Troopers.upgrade(prisma));
  app.post("/api/trooper/chooseSkill", Troopers.chooseSkill(prisma));
  app.post("/api/trooper/add", Troopers.add(prisma));
};

export default initRoutes;
