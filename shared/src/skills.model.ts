import {
  HelmetName,
  ItemName,
  PerkName,
  Trooper,
  VehicleName,
  WeaponName,
} from "@minitroopers/prisma";

export type SkillName =
  | WeaponName
  | PerkName
  | ItemName
  | VehicleName
  | HelmetName;

const allSkillArrayWithoutHelmet = [
  ...Object.values(WeaponName),
  ...Object.values(PerkName),
  ...Object.values(ItemName),
  ...Object.values(VehicleName),
  // ...Object.values(HelmetName), // not in choice
];

export interface WeaponSkill {
  damage: string; // 4  2-3
  range: string; // 4  2-3
  critical: number;
  aim: number;
  recovery: number;
  skills?: string;
  bursts?: number;
  type: "weapon";
  name: WeaponName;
}

export interface PerkSkill {
  description: string;
  quote: string;
  type: "perk";
  name: SkillName;
}

export interface ItemSkill {
  type: "item";
  name: ItemName;
}

export interface vehiculeSkill {
  type: "vehicule";
  name: VehicleName;
}

export interface HelmetSkill {
  type: "helmet";
  name: HelmetName;
}

const SkillMinLevel: Readonly<Partial<Record<SkillName, number>>> = {
  // helmet
  // commsofficer: 6,
  // doctor: 6,
  // munitions: 6,
  // pilot: 6,
  // saboteur: 6,
  // scout: 6,
  // soldier: 6,
  // spy: 6,
  //  vehicle
  motorcycle: 4,
  lighttank: 7,
  heavytank: 7,
  helicopter: 7,
  fighterjet: 12,
  // ammunition
  armorpiercingshells: 7,
  // grenade
  clowngrenade: 5,
  grenadebenie: 7,
  healinggrenade: 7,
  blackholegrenade: 13,
  // other
  charge: 7,
  anatomy: 7,
  blindfury: 7,
  coveringfire: 7,
  tailgunner: 2,
  nervous: 5,
  stamp: 5,
  deathgrip: 5,
  lastmohican: 5,
  hyperactive: 5,
  amphetamineshot: 5,
  wifebeater: 7,
  hurry: 7,
  bounceback: 7,
  rucksack: 7,
  restless: 7,
  martyr: 7,
  binoculars: 7,
  radio: 7,
  talkywalky: 7,
  outofbounds: 7,
  reverseattack: 7,
  commander: 7,
  occupation: 7,
  saviour: 7,
  scavenger: 5,
  voodoodoll: 7,
  heavyweight: 5,
  fallguy: 5,
  invincible: 5,
  suspicious: 7,
  friendlyfire: 7,
  crybaby: 7,
  interception: 7,
  survivor: 7,
  firstaid: 7,
};

const SkillNeededHelmet: Readonly<Partial<Record<SkillName, SkillName>>> = {
  anatomy: "doctor",
  amphetamineshot: "doctor",
  healinggrenade: "doctor",
  binoculars: "scout",
  saviour: "doctor",
};

export const pickSkill = (trooper?: Trooper): SkillName => {
  const pool = createSkillPool(trooper);
  return pool[Math.floor(Math.random() * pool.length)];
};

const createSkillPool: (trooper?: Trooper) => SkillName[] = (
  trooper?: Trooper
) => {
  let pool: SkillName[] = [];

  const blackListedSkill: SkillName[] = [PerkName.commander]; // BlackListed SKills

  const alreadySkills = new Set<SkillName>();
  if (trooper) {
    if (trooper.helmet) alreadySkills.add(trooper.helmet);
    [
      ...trooper.items,
      ...trooper.skills,
      ...trooper.weapons,
      ...trooper.vehicle,
    ].forEach((skill) => alreadySkills.add(skill));
  }

  if (trooper?.level === 6) {
    pool = Object.values(HelmetName);
  } else {
    for (const skill of allSkillArrayWithoutHelmet) {
      if (
        !blackListedSkill.includes(skill) &&
        !alreadySkills.has(skill) &&
        (trooper?.level ?? 1) >= (SkillMinLevel[skill] ?? 0) &&
        (!SkillNeededHelmet[skill] ||
          alreadySkills.has(SkillNeededHelmet[skill] as SkillName))
      ) {
        pool.push(skill);
      }
    }

    // + apply modifier weight skill based trooper
    // + remove already existing
  }

  return pool;
};

// https://minitroopers.fandom.com/wiki/Category:Weapon
export const WeaponAttributes = [
  "damage",
  "range",
  "critical",
  "burst",
  "aim",
  "recovery",
  "capacity",
  "shots",
  "skills",
];

export type WeaponAttribute = (typeof WeaponAttributes)[number];
