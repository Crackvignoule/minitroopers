import { HistoryType, HistoryUser, Trooper, User } from "@minitroopers/prisma";

export interface UserWithTroopers extends User {
  troopers: Trooper[];
  history: HistoryUser[];
}

export type PartialUserWithTroopers = Omit<
  UserWithTroopers,
  | "id"
  | "lang"
  | "name"
  | "invitedFrom"
  | "createdAt"
  | "lastConnexion"
  | "admin"
  | "connexionToken"
>;

export const getReferralPrice = (recruit: number): number => {
  if (recruit >= 35) return 0;
  if (recruit >= 15) return 5;
  if (recruit >= 5) return 10;
  if (recruit >= 1) return 50;
  return 100;
};

// export const getPower = (user: UserWithTroopers): number => {
//   return (
//     4 * user.troopers.length +
//     user.troopers.reduce((acc, curr) => {
//       acc += curr.level;
//       return acc;
//     }, 0)
//   );
// };

export interface FightHistory extends HistoryUser {
  options: {
    result: "victory" | "defeat";
    fightId: number;
    enemyName: string;
    clickable: true;
  };
}
