export type ArmyColor =
  | "#E87D5F"
  | "#FFAA27"
  | "#FFDB7F"
  | "#96B732"
  | "#9EADEF"
  | "#A55DC6";

export const ArmyColorArray: ArmyColor[] = [
  "#E87D5F",
  "#FFAA27",
  "#FFDB7F",
  "#96B732",
  "#9EADEF",
  "#A55DC6",
];

export const getUpgradeCost = (level: number): number => {
  return Math.floor(Math.sqrt(Math.pow(level, 5)));
};

export const getAddCost = (trooperLength: number): number => {
  if (trooperLength > 11) {
    return 0;
  }
  return Math.floor(6 * Math.pow(trooperLength, 2)) + 2;
};
